#ifndef MAX6675_C_h
#define MAX6675_C_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#ifndef MAX6675_SO_DDR
#define MAX6675_SO_DDR      DDRD
#endif
#ifndef MAX6675_SO_PORT
#define MAX6675_SO_PORT     PORTD
#endif
#ifndef MAX6675_SO_PIN
#define MAX6675_SO_PIN      PIND
#endif
#ifndef MAX6675_SO_BIT
#define MAX6675_SO_BIT      PD0
#endif

#ifndef MAX6675_CS_DDR
#define MAX6675_CS_DDR      DDRD
#endif
#ifndef MAX6675_CS_PORT
#define MAX6675_CS_PORT     PORTD
#endif
#ifndef MAX6675_CS_BIT
#define MAX6675_CS_BIT      PD1
#endif

#ifndef MAX6675_SCK_DDR
#define MAX6675_SCK_DDR     DDRD
#endif
#ifndef MAX6675_SCK_PORT
#define MAX6675_SCK_PORT    PORTD
#endif
#ifndef MAX6675_SCK_BIT
#define MAX6675_SCK_BIT     PD2
#endif

    /**
     * Return temperature in uint16_t in 1/4 Celsius
     * 
     * @param uint16_t *temperature
     * @return Status   0x00 - OK
     *                  0x01 - incorrect data
     *                  0x02 - the thermocouple input is open
     */
    uint8_t max6675_temperatureInt(uint16_t *temperature);

    /**
     * Return temperature in float in Celsius
     * 
     * @param uint16_t *temperature
     * @return Status   0x00 - OK
     *                  0x01 - incorrect data
     *                  0x02 - the thermocouple input is open
     */
    uint8_t max6675_temperatureFloat(float *temperature);
#ifdef __cplusplus
}
#endif

#endif
