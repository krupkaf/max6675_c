#include "MAX6675_C.h"
#include <avr/io.h>
#include <util/delay.h>

uint16_t max6675_spi(void) {
    uint8_t i;
    uint16_t d = 0;

    MAX6675_CS_PORT |= _BV(MAX6675_CS_BIT);
    MAX6675_CS_DDR |= _BV(MAX6675_CS_BIT);

    MAX6675_SCK_PORT |= _BV(MAX6675_SCK_BIT);
    MAX6675_SCK_DDR |= _BV(MAX6675_SCK_BIT);

    MAX6675_SO_DDR &= ~_BV(MAX6675_SO_BIT);
    MAX6675_SO_PORT |= _BV(MAX6675_SO_BIT);

    _delay_us(10);

    MAX6675_CS_PORT &= ~_BV(MAX6675_CS_BIT);

    for (i = 0; i < 16; i++) {
        MAX6675_SCK_PORT &= ~_BV(MAX6675_SCK_BIT);
        _delay_us(1);
        d <<= 1;
        if (MAX6675_SO_PIN & _BV(MAX6675_SO_BIT)) {
            d |= 0x0001;
        }

        MAX6675_SCK_PORT |= _BV(MAX6675_SCK_BIT);
        _delay_us(1);
    }

    MAX6675_CS_PORT |= _BV(MAX6675_CS_BIT);

    return d;
}

uint8_t max6675_temperatureInt(uint16_t *temperature) {
    uint16_t tmp;

    tmp = max6675_spi();
    if (tmp & 0x8002) {
        //incorrect data
        return 0x0001;
    }
    if (tmp & 0x04) {
        //the thermocouple input is open.
        return 0x02;
    }
    *temperature = tmp >> 3;
    return 0;
}

uint8_t max6675_temperatureFloat(float *temperature) {
    uint16_t tmp;
    uint8_t status;

    status = max6675_temperatureInt(&tmp);
    if (status == 0x00) {
        *temperature = (float) tmp * 0.25;
    }
    return status;
}
