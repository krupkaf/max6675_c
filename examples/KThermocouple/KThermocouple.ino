#include <MAX6675_C.h>

void setup() {
    Serial.begin(9600);
}

void loop() {
    uint16_t temperature;
    float temperaturef;
    uint8_t status;

    status = max6675_temperatureInt(&temperature);
    if (status == 0x00) {
        Serial.print(F("Int temperature in Celsius: "));
        Serial.print(temperature / 4);
    } else {
        if (status & 0x01) {
            Serial.print(F("Incorrect data!"));
        }
        if (status & 0x02) {
            Serial.print(F("Thermocouple input is open!"));
        }
    }
    Serial.println();

    status = max6675_temperatureFloat(&temperaturef);
    if (status == 0x00) {
        Serial.print(F("Float temperature in Celsius: "));
        Serial.print(temperaturef, 2);
    } else {
        if (status & 0x01) {
            Serial.print(F("Incorrect data!"));
        }
        if (status & 0x02) {
            Serial.print(F("Thermocouple input is open!"));
        }
    }
    Serial.println();

    Serial.println();
    delay(1000);
}
